include_directories(${CMAKEDEMO_SOURCE_DIR}/Logic)
include_directories(${CMAKEDEMO_SOURCE_DIR}/SDL)

set(SRC 	Logic/PlayGame.cpp
		Logic/AbstractFactory.cpp 		
		Logic/Ball.cpp 
		Logic/Breakable.cpp
		Logic/Entity.cpp	
		Logic/Vector2.cpp
		Logic/Paddle.cpp
		Logic/Level.cpp			
		Logic/Stopwatch.cpp
		Logic/LevelGenerator.cpp
		Logic/Game.cpp		
		Logic/MovableEntity.cpp
		Logic/Player.cpp		
		Logic/PowerUp.cpp
		SDL/SDLPowerUp.cpp
		SDL/SDLResource.cpp 
		SDL/SDLFactory.cpp 
		SDL/SDLBall.cpp 
		SDL/SDLBreakable.cpp 
		SDL/SDLPaddle.cpp
		SDL/SDLLevel.cpp
		SDL/SDLPlayer.cpp)

# Required libraries:
# (note that on systems with nonstandard SDL location, SDLDIR=/path/to/libsdl
# and SDLIMAGEDIR=/path/to/libsdlimage/include/SDL should be defined as exported shell variables)
find_package(SDL REQUIRED)
find_package(SDL_image REQUIRED)
find_package(SDL_ttf REQUIRED)
find_package(SDL_mixer REQUIRED)

# Add include dirs for SDL & SDL_Image
include_directories(${SDL_INCLUDE_DIR})
include_directories(${SDLIMAGE_INCLUDE_DIR})
include_directories(${SDLTTF_INCLUDE_DIR})
include_directories(${SDLMIXER_INCLUDE_DIR})


# Add library dirs for SDL & SDL_Image
set(LIBS ${LIBS} ${SDL_LIBRARY})
set(LIBS ${LIBS} ${SDLIMAGE_LIBRARY})
set(LIBS ${LIBS} ${SDLTTF_LIBRARY})
set(LIBS ${LIBS} ${SDLMIXER_LIBRARY})


# The binaries to be built:
add_executable(Breakout ${SRC})

# Link against LIBS
target_link_libraries(Breakout ${LIBS})

# Install rules:
install(TARGETS Breakout DESTINATION ${PROJECT_SOURCE_DIR}/bin)


# add a target to generate API documentation with Doxygen
find_package(Doxygen)
if(DOXYGEN_FOUND)
	configure_file(../src/Doxyfile.in ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile @ONLY)	
	add_custom_target(doc ALL
	${DOXYGEN_EXECUTABLE} ../src/Doxyfile
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)
endif(DOXYGEN_FOUND)
