/**
* @file SDLFactory.h
* SDLFactory class declaration
* @author Timmy Nelen
* @date 27/11/2010
*/

#ifndef SDLFACTORY_H_
#define SDLFACTORY_H_

#include "../Logic/AbstractFactory.h"
#include "SDLBall.h"
#include "SDLBreakable.h"
#include "SDLPaddle.h"
#include "SDLPowerUp.h"
#include "SDLLevel.h"
using namespace bo_sdl;

namespace bo_sdl{

/**
* @class SDLFactory
* @brief SDLFactory, the factory pattern used to create SDLEntities
* @author Timmy Nelen
* @date 27/11/2010
*/
class SDLFactory: public AbstractFactory {
public:
	SDLFactory();
	~SDLFactory();
	SDLBall* CreateBall(const char* fileName, double xPos, double yPos, double xVel, double yVel);
	SDLBreakable* CreateBreakable(const char* fileName, double xPos, double yPos);
	SDLPaddle* CreatePaddle(const char* fileName, double xPos, double yPos, const double vel);
	SDLPowerUp* CreatePowerUp(const char* fileName, double xPos, double yPos, const double vel, const int probability);
	double GetBreakableWidth(const char* breakableFile);
	Level* GetLevel();
private:
	SDL_Surface* fBallSprite;
	SDL_Surface* fPaddleSprite;
	SDL_Surface* fPowerUpSprite;
	SDL_Surface* fBreakableSprite;
};
}
#endif /* SDLFACTORY_H_ */
