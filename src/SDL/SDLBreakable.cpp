/**
* @file SDLBreakable.cpp
* SDLBreakable implementation
* @author Timmy Nelen
* @date 18/11/2010
*/

#define res SDLResource::Instance()

#include "SDLBreakable.h"

/**
 * Constructor for an SDLBreakable object
 *
 * @param[in] x The X position of the SDLBreakable
 * @param[in] y The Y position of the SDLBreakable
 * @param[in] rad The radius of the SDLBall
 * @param[in] img The sprite to be used
 */
bo_sdl::SDLBreakable::SDLBreakable(const double x, const double y, const double rad, SDL_Surface* img)
	: Breakable(x,y,rad){
	this->fSprite = img;
}

/**
 * The destructor for the SDLBreakable object
 */
bo_sdl::SDLBreakable::~SDLBreakable() {
	Vector2 newPos = this->GetPos().ConvToScreenCoords(res->GetScale()) - res->GetScale() * this->GetRadius();
	res->BlitToScreen(res->GetPow(), newPos);
	delete fSprite;
}


/**
 * Converts the virtual screen position to the real one and blits the surface on the screen
 */
void bo_sdl::SDLBreakable::Visualize(){
	Vector2 newPos = this->GetPos().ConvToScreenCoords(res->GetScale()) - res->GetScale()* this->GetRadius();
	res->BlitToScreen(fSprite, newPos);
}

