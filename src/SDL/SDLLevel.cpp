/**
* @file SDLLevel.cpp
* SDLLevel implementation
* @author Timmy Nelen
* @date 6/12/2010
*/

#include "SDLLevel.h"
#include "SDLResource.h"

#define res SDLResource::Instance()

/**
 * Stub procedure
 */
bo_sdl::SDLLevel::SDLLevel() {

}

/**
 * Stub procedure
 */
bo_sdl::SDLLevel::~SDLLevel() {
}

/**
 * Processes the input through SDL
 * @return -1: No key pressed, 0: Key Left (iff key right is not pressed), 1: Key Right, 2: Escape
 */
int bo_sdl::SDLLevel::ProcessLevelInput(){
	Uint8* keyState;
	SDL_EnableKeyRepeat(0,0);
	SDL_PumpEvents();
	keyState = SDL_GetKeyState(0);
	if ( keyState[SDLK_LEFT])
		return 0;
	else if ( keyState[SDLK_RIGHT] )
		return 1;
	else if ( keyState[SDLK_ESCAPE] )
		return 2;
	else
		return -1;
}
