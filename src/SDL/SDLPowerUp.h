/**
* @file SDLPowerUp.h
* SDLPowerUp declaration
* @author Timmy Nelen
* @date 13/12/2010
*/

#ifndef SDLPOWERUP_H_
#define SDLPOWERUP_H_

#include "../Logic/PowerUp.h"
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

namespace bo_sdl {

/**
* @class SDLPowerUp
* @brief SDLPowerUp, based on PowerUp, represents the visual representation of powerups used in the game
* @author Timmy Nelen
* @date 13/12/2010
*/
class SDLPowerUp: public bo::PowerUp {
public:
	SDLPowerUp(const double x, const double y, const double rad, const double vel, const int probability, SDL_Surface* img);
	virtual ~SDLPowerUp();
	void Visualize();
private:
	SDL_Surface* fSprite;
};

}

#endif /* SDLPOWERUP_H_ */
