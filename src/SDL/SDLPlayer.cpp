/**
* @file SDLPlayer.cpp
* SDLPlayer implementation
* @author Timmy Nelen
* @date 12/12/2010
*/

#include "SDLPlayer.h"
#include "SDLResource.h"
#define res SDLResource::Instance()

/**
 * The constructor for an SDLPlayer object
 *
 * @param[in] playerName The name of the player
 * @param[in] amOfLives The amount of lives
 */
bo_sdl::SDLPlayer::SDLPlayer(const char* playerName, const int amOfLives)
	:Player(playerName, amOfLives){

}

/**
 * The visualization method of the SDLPlayer, also flips & clears the back buffer as it is the last element to be visualized
 */
void bo_sdl::SDLPlayer::Visualize(){
	res->DrawHUDRect();
	string finalStr = "Player name: " + string(this->GetName()) + " Score: " + string(this->GetScoreString())
							+ " Lives left: " + string(this->GetLivesLeftString());
	if ( this->GetPowerUpActive() )
		finalStr += " POWERUP ACTIVE: 4X SPEED!";
	res->DrawText(finalStr.c_str(), 0.1, 5.8);
	res->FlipScreen();
	res->ClearBackBuffer();
}
