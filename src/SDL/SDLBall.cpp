/**
* @file SDLBall.cpp
* SDLBall implementation
* @author Timmy Nelen
* @date 18/11/2010
*/
#define res SDLResource::Instance()

#include "SDLBall.h"
#include "SDLResource.h"

/**
 * Constructor for an SDLBall object
 *
 * @param[in] x The X position of the SDLBall
 * @param[in] y The Y position of the SDLBall
 * @param[in] rad The radius of the SDLBall
 * @param[in] xVel The X component of the SDLBall's velocity
 * @param[in] yVel The Y component of the SDLBall's velocity
 * @param[in] img The sprite to be used
 */
bo_sdl::SDLBall::SDLBall(const double x, const double y, const double rad, const double xVel, const double yVel, SDL_Surface* img)
	: Ball(x, y, rad, xVel, yVel){
	this->fBallSprite = img;
}

/**
 * Destructor for the SDLBall object
 */
bo_sdl::SDLBall::~SDLBall() {
	delete fBallSprite;
}

/**
 * Converts the virtual position to the "real position" on the screen, and blits the set sprite
 */
void bo_sdl::SDLBall::Visualize(){
	Vector2 newPos(this->GetPos().GetX(), this->GetPos().GetY());
	newPos.ConvToScreenCoords(res->GetScale());
	newPos - res->GetScale()*this->GetRadius();
	res->BlitToScreen(this->fBallSprite, newPos);
}

