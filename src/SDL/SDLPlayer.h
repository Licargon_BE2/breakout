/**
* @file SDLPlayer.h
* SDLPlayer class declaration
* @author Timmy Nelen
* @date 12/12/2010
*/

#ifndef SDLPLAYER_H_
#define SDLPLAYER_H_

#include "../Logic/Player.h"
#include <string>
using std::string;

namespace bo_sdl {

/**
* @class SDLPlayer
* @brief SDLPlayer, based on player, represents the visual representation of the player class
* @author Timmy Nelen
* @date 12/12/2010
*/
class SDLPlayer: public bo::Player {
public:
	SDLPlayer(const char* playerName, const int amOfLives);
	void Visualize();
};

}

#endif /* SDLPLAYER_H_ */
