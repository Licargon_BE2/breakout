/**
* @file SDLPaddle.cpp
* SDLPaddle implementation
* @author Timmy Nelen
* @date 18/11/2010
*/

#define res SDLResource::Instance()

#include "SDLPaddle.h"

/**
 * Constructor for an SDLPaddle object
 *
 * @param[in] x The X position of the object
 * @param[in] y The Y position of the object
 * @param[in] rad The radius of the object
 * @param[in] vel The velocity of the object
 * @param[in] img The sprite to be used
 */
bo_sdl::SDLPaddle::SDLPaddle(const double x, const double y, const double rad, const double vel, SDL_Surface* img)
	: Paddle(x, y, rad, vel){
	this->fSprite = img;
}


/**
 * The destructor for the SDLPaddle pbjects
 */
bo_sdl::SDLPaddle::~SDLPaddle() {
	delete fSprite;
}
/**
 * Converts the virtual screen position to the real one and blits the surface on the screen
 */
void bo_sdl::SDLPaddle::Visualize(){
	Vector2 newPos = this->GetPos().ConvToScreenCoords(res->GetScale()) - this->GetRadius()*res->GetScale();
	res->BlitToScreen(fSprite, newPos);
	//res->FlipScreen();
	//res->ClearBackBuffer();
}
