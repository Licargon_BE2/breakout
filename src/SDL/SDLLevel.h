/**
* @file SDLLevel.h
* SDLLevel class declaration
* @author Timmy Nelen
* @date 6/12/2010
*/

#ifndef SDLLEVEL_H_
#define SDLLEVEL_H_

#include "../Logic/Level.h"

namespace bo_sdl{

/**
* @class SDLLevel
* @brief SDLLevel, based on Level, is an SDL Expansion to the level class created to intercept keyboard input through SDL
* @author Timmy Nelen
* @date 6/12/2010
*/
class SDLLevel: public bo::Level {
public:
	SDLLevel();
	virtual ~SDLLevel();
	virtual int ProcessLevelInput();
};
}

#endif /* SDLLEVEL_H_ */
