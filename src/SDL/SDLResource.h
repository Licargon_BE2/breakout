/**
* @file SDLResource.h
* SDLResource ceclaration
* @author Timmy Nelen
* @date 21/11/2010
*/

#ifndef SDLRESOURCE_H_
#define SDLRESOURCE_H_

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "../Logic/Vector2.h"
#include <SDL/SDL_ttf.h>

namespace bo_sdl{

/**
* @class SDLResource
* @brief SDL wrapper class to easily work with SDL
* @author Timmy Nelen
* @date 21/11/2010
*/
class SDLResource {
public:
	static SDLResource* Instance();
	SDLResource();
	virtual ~SDLResource();
	bool Initialize(int xScreen, int yScreen, double scale, const char* bgFile, const char* powFile);
	void BlitToScreen(SDL_Surface*, Vector2);
	void FlipScreen();
	void ClearBackBuffer();
	void Display();
	double GetScale();
	void SetBackground(const char* fileName);
	void BlitBackground();
	void DrawHUDRect();
	void DrawText(const char* string, const double x, const double y);
	void ShowMenu(int& returnValue);
	void ShowHelpMenu();
	SDL_Surface* GetPow();
private:
	SDL_Surface* fScreen;
	static SDLResource* fInstance;
	double fScale;
	SDL_Surface* fBackground;
	bool fBGBlitted;
	TTF_Font* fFont;
	SDL_Surface* fPow;

};

}

#endif /* SDLRESOURCE_H_ */
