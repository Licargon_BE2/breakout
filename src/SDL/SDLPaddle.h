/**
* @file SDLPaddle.h
* SDLPaddle class declaration
* @author Timmy Nelen
* @date 18/11/2010
*/

#ifndef SDLPADDLE_H_
#define SDLPADDLE_H_

#include "../Logic/Paddle.h"
#include "SDLResource.h"
#include "../Logic/Vector2.h"
using namespace bo;

namespace bo_sdl {

/**
* @class SDLPaddle
* @brief SDLPaddle, based on Paddle, represents the visual representation of the paddle used ingame
* @author Timmy Nelen
* @date 18/11/2010
*/
class SDLPaddle: public Paddle {
public:
	SDLPaddle(const double x, const double y, const double rad, const double vel, SDL_Surface* img);
	virtual ~SDLPaddle();
	void Visualize();
private:
	SDL_Surface* fSprite;
};

}

#endif /* SDLPADDLE_H_ */
