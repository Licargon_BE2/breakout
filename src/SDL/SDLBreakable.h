/**
* @file SDLBreakable.h
* SDLBreakable class declaration
* @author Timmy Nelen
* @date 18/11/2010
*/

#ifndef SDLBREAKABLE_H_
#define SDLBREAKABLE_H_

#include "../Logic/Breakable.h"
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "SDLResource.h"
#include "../Logic/Vector2.h"
using namespace bo;

namespace bo_sdl {

/**
* @class SDLBreakable
* @brief SDLBreakable, based on Breakable, represents the visuals used for the breakable objects
* @author Timmy Nelen
* @date 18/11/2010
*/
class SDLBreakable: public Breakable {
public:
	SDLBreakable(const double x, const double y, const double rad, SDL_Surface* img);
	virtual ~SDLBreakable();
	void Visualize();
private:
	SDL_Surface* fSprite;
};

}

#endif /* SDLBREAKABLE_H_ */
