/**
* @file SDLBall.h
* SDLBall class declaration
* @author Timmy Nelen
* @date 18/11/2010
*/

#ifndef SDLBALL_H_
#define SDLBALL_H_

#include "../Logic/Ball.h"
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "SDLResource.h"
#include "../Logic/Vector2.h"

namespace bo_sdl {

/**
* @class SDLBall
* @brief SDLBall, based on Ball, represents the visual representation of the ball used in the game
* @author Timmy Nelen
* @date 18/11/2010
*/
class SDLBall: public bo::Ball {
public:
	SDLBall(const double x, const double y, const double rad, const double xVel, const double yVel, SDL_Surface* img);
	virtual ~SDLBall();
	void Visualize();
private:
	SDL_Surface* fBallSprite;
};

}

#endif /* SDLBALL_H_ */
