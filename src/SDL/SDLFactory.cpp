/**
* @file SDLFactory.cpp
* SDLFactory implementation
* @author Timmy Nelen
* @date 27/11/2010
*/

#include "SDLFactory.h"
#include "SDLResource.h"

#define res SDLResource::Instance()

/**
 * Empty constructor
 */
bo_sdl::SDLFactory::SDLFactory(){
}

/**
 * Empty destructor
 */
bo_sdl::SDLFactory::~SDLFactory(){ }

/**
 * Creates an SDL Ball object with the supplied position, sprite and velocity
 *
 * @param[in] fileName The filename of the sprite to be used
 * @param[in] xPos The X position of the object
 * @param[in] yPos The Y position of the object
 * @param[in] xVel The X component of the object's velocity
 * @param[in] yVel The Y component of the object's velocity
 *
 * @return Ptr to SDLBall object with the supplied attributes
 */
SDLBall* bo_sdl::SDLFactory::CreateBall(const char* fileName, double xPos, double yPos, double xVel, double yVel){
	SDL_Surface* img = IMG_Load(fileName);
	SDLBall* ball = new SDLBall(xPos, yPos, (img->w / 2) / res->GetScale(), xVel, yVel, img);
	return ball;
}

/**
 * Creates an SDL Breakable object with the supplied position, sprite and velocity
 *
 * @param[in] fileName The filename of the sprite to be used
 * @param[in] xPos The X position of the object
 * @param[in] yPos The Y position of the object
 *
 * @return Ptr to SDLBreakable object with the supplied attributes
 */
SDLBreakable* bo_sdl::SDLFactory::CreateBreakable(const char* fileName, double xPos, double yPos){
	SDL_Surface* img = IMG_Load(fileName);
	SDLBreakable* breakable = new SDLBreakable(xPos, yPos, (img->w / 2 ) / res->GetScale(), img);
	return breakable;
}

/**
 * Creates an SDL Paddle object with the supplied position, sprite and velocity
 *
 * @param[in] fileName The filename of the sprite to be used
 * @param[in] xPos The X position of the object
 * @param[in] yPos The Y position of the object
 * @param[in] vel The object's velocity
 *
 * @return Ptr to SDLPaddle object with the supplied attributes
 */
SDLPaddle* bo_sdl::SDLFactory::CreatePaddle(const char* fileName, double xPos, double yPos, const double vel){
	SDL_Surface* img = IMG_Load(fileName);
	SDLPaddle* pad = new SDLPaddle(xPos, yPos, (img->w / 2) / res->GetScale(), vel, img);
	return pad;
}

/**
 * Creates an SDL Powerup object with the supplied position, sprite and velocity
 *
 * @param[in] fileName The filename of the sprite to be used
 * @param[in] xPos The X position of the object
 * @param[in] yPos The Y position of the object
 * @param[in] vel The object's velocity
 * @param[in] probability The probability that the powerup will be released
 *
 * @return Ptr to SDLPowerUp object with the supplied attributes
 */
SDLPowerUp* bo_sdl::SDLFactory::CreatePowerUp(const char* fileName, double xPos, double yPos, const double vel, const int probability){
	SDL_Surface* img = IMG_Load(fileName);
	SDLPowerUp* pUp = new SDLPowerUp(xPos, yPos, (img->w / 2) / res->GetScale(), vel, probability, img);
	return pUp;
}

/**
 * Returns the width of a breakable object
 *
 * @param[in] fileName The filename of the breakable object
 *
 * @return The width of the supplied breakable sprite
 */
double bo_sdl::SDLFactory::GetBreakableWidth(const char* breakableFile){
	SDL_Surface* temp = IMG_Load(breakableFile);
	return ( temp->w / res->GetScale() );
}

/**
 * Creates a new empty SDLLevel object
 *
 * @return Empty SDLLevel object
 */
Level* bo_sdl::SDLFactory::GetLevel(){
	SDLLevel* temp = new SDLLevel;
	return temp;
}
