/**
* @file SDLPowerUp.cpp
* SDLPowerUp implementation
* @author Timmy Nelen
* @date 13/12/2010
*/


#define res SDLResource::Instance()

#include "SDLPowerUp.h"
#include "SDLResource.h"
#include <list>
using namespace std;

/**
 * The constructor for an SDLPowerup object
 *
 * @param[in] x The X position of the object
 * @param[in] y The Y position of the object
 * @param[in] rad The radius of the object
 * @param[in] vel The velocity of the object
 * @param[in] probability The probability the powerup will be released
 * @param[in] img The sprite to be used
 */
bo_sdl::SDLPowerUp::SDLPowerUp(const double x, const double y, const double rad, const double vel, const int probability,  SDL_Surface* img)
	:PowerUp(x, y, rad, vel, probability){
	this->fSprite = img;

}

/**
 * The destructor of the SDLPowerUp object
 */
bo_sdl::SDLPowerUp::~SDLPowerUp() {
	delete fSprite;
}

/**
 * Visualization method of the SDLPowerUp
 */
void bo_sdl::SDLPowerUp::Visualize(){
	list<Vector2> locList = this->GetLocations();
	for (list<Vector2>::iterator it = locList.begin(); it!=locList.end(); ++it){
		Vector2 newPos = (*it);
		newPos.ConvToScreenCoords(res->GetScale());
		newPos - res->GetScale()*this->GetRadius();
		res->BlitToScreen(this->fSprite, newPos);
	}
}
