/**
 * @file Ball.h
 * Header file for the Ball class
 * @author Timmy Nelen
 * @date 18/11/2010
 */

#ifndef BALL_H_
#define BALL_H_

#include "MovableEntity.h"
#include "Vector2.h"
#include <iostream>
#include <cmath>

namespace bo {

/**
 * @class Ball
 * @brief Ball class, based on the MovableEntity class
 * @author Timmy Nelen
 * @date 18/11/2010
 */
class Ball: public bo::MovableEntity {
public:
	Ball(const double x, const double y, const double xVel, const double yVel, const double radius);
	virtual ~Ball();
	virtual void Visualize() = 0;
	void Bounce(Entity*);
	void AdvanceBall();
	void SetDoubleVelocity(const double xVel, const double yVel);
	double GetXVelocity();
	double GetYVelocity();
private:
	double fXVel, fYVel;
};

}

#endif /* BALL_H_ */
