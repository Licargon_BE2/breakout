/**
* @file MovableEntity.cpp
* MovableEntity base class implementation
* @author Timmy Nelen
* @date 9/12/2010
*/


#include "MovableEntity.h"

/**
 * Constructor of a movable entity
 *
 * @param[in] x The X position of the entity
 * @param[in] y The Y position of the entity
 * @param[in] rad The radius of the entity
 */
bo::MovableEntity::MovableEntity(const double x, const double y, const double rad)
	: Entity(x, y, rad){

}

/**
 * (Empty) destructor of the MovableEntity object
 */
bo::MovableEntity::~MovableEntity() {
	// TODO Auto-generated destructor stub
}

