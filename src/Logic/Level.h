/*
 * Level.h
 *
 *  Created on: Nov 18, 2010
 *      Author: timmeh
 */

/**
* @file Level.h
* Level Class Declaration
* @author Timmy Nelen
* @date 18/11/2010
*/

#ifndef LEVEL_H_
#define LEVEL_H_

#include "Entity.h"
#include "Ball.h"
#include "Breakable.h"
#include "Paddle.h"
#include <list>
#include "Vector2.h"
#include "Player.h"
#include "PowerUp.h"

namespace bo {

/**
* @class Level
* @brief Declaration of the level class
* @author Timmy Nelen
* @date 18/11/2010
*/
class Level {
public:
	Level();
	virtual ~Level();
	void AddToLevel(Entity*);
	virtual void Visualize();
	void CheckCollisions(Player* player);
	void AddBall(Ball*);
	void AddPaddle(Paddle*);
	void AdvanceLevelState(int& levelReturn, Player* player);
	void ReleasePowerUp(Vector2 pos);
	void AddPowerUp(PowerUp* powerUp);
private:
	Ball* fBall;
	Paddle* fPaddle;
	virtual int ProcessLevelInput(){return -1;};
	list<Entity*> fEntList;
	list<PowerUp*> fPowerUpList;
};

}

#endif /* LEVEL_H_ */
