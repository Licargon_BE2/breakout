/**
* @file Player.cpp
* Player Class implementation
* @author Timmy Nelen
* @date 12/12/2010
*/

#include "Player.h"

/**
 * Constructor, sets the name, amount of lives, ...
 */
bo::Player::Player(const char* name, const int amOfLives) {
	this->fName = name;
	this->fScore = 0;
	this->fLives = amOfLives;
	this->fPowerUpActive = 0;
}

/**
 * Adds a supplied amount of points to the player
 *
 * @param[in] points The points to be added
 */
void bo::Player::AddPoints(const int points){
	this->fScore += points;
}

/**
 * Returns how many lives the player has left
 *
 * @return The amount of lives the player has left
 */
int bo::Player::GetLivesLeft(){
	return this->fLives;
}

/**
 * Substracts 1 live from the player
 */
void bo::Player::DecLives(){
	--this->fLives;
}

/**
 * Returns the player name
 *
 * @return The name of the player
 */
const char* bo::Player::GetName(){
	return this->fName;
}

/**
 * Return value = true when there is a powerup active
 *
 * @return True if a powerup is active, false otherwise
 */
bool bo::Player::GetPowerUpActive(){
	return this->fPowerUpActive;
}

/**
 * If a powerup is picked up, this gets called to set that the player has a powerup active
 */
void bo::Player::SetPowerUpActive(){
	this->fPowerUpActive = true;
	this->fPrevTime = clock();
}

/**
 * Advances the powerup timer, sets the powerup status to false once 10 seconds has been reached
 *
 * @param[in,out] returnVal The returnvalue of the function, =1 if the powerup has to be disabled
 */
void bo::Player::AdvancePowerUpTimer(int& returnVal){
	if ( ( clock () - this->fPrevTime ) / CLOCKS_PER_SEC > 10){
		this->fPowerUpActive = false;
		returnVal = 1;
	}
	else{
		returnVal = 0;
	}
		return;
}

/**
 * Returns a string containing the score of the player
 *
 * @return The string containing the playername
 */
const char* bo::Player::GetScoreString(){
	std::ostringstream stream;
	stream << this->fScore;
	std::string temp = stream.str();
	return temp.c_str();
}

/**
 * Returns a string containing the amount of lives of the player
 *
 * @return The string containing the lives left of the player
 */
const char* bo::Player::GetLivesLeftString(){
	std::ostringstream stream;
	stream << this->fLives;
	std::string temp = stream.str();
	return temp.c_str();
}

/**
 * Returns the player's score
 *
 * @return The score of the player
 */
int bo::Player::GetScore(){
	return this->fScore;
}

/**
 * Destructor for the player object
 */
bo::Player::~Player() {
	// TODO Auto-generated destructor stub
}

