/**
* @file Paddle.h
* Paddle class, based on MovableEntity declaration
* @author Timmy Nelen
* @date 18/11/2010
*/

#ifndef PADDLE_H_
#define PADDLE_H_

#include "MovableEntity.h"


namespace bo {

/**
* @class Paddle
* @brief Paddle class, represents the used paddle in the game
* @author Timmy Nelen
* @date 18/11/2010
*/
class Paddle: public bo::MovableEntity {
public:
	Paddle(const double x, const double y, const double rad, const double vel);
	virtual ~Paddle();
	virtual void Visualize() = 0;
	void SetSingleVelocity(const double vel);
	void UpdatePos(int direction);
	double GetVelocity();
private:
	double fVelocity;
};

}

#endif /* PADDLE_H_ */
