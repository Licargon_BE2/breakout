/**
* @file Breakable.h
* Breakable Object declaration
* @author Timmy Nelen
* @date 18/11/2010
*/

#ifndef BREAKABLE_H_
#define BREAKABLE_H_

#include "Entity.h"

namespace bo {

/**
* @class Breakable
* @brief Breakable Class, based on the Entity class
* @author Timmy Nelen
* @date 18/11/2010
*/
class Breakable: public bo::Entity {
public:
	Breakable(const double x, const double y, const double rad);
	virtual ~Breakable();
	virtual void Visualize() = 0;
};

}

#endif /* BREAKABLE_H_ */
