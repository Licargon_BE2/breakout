/**
* @file Entity.cpp
* Base Entity Class declaration
* @author Timmy Nelen
* @date 18/11/2010
*/

#include "Entity.h"

/**
 * Constructor of an entity object
 *
 * @param[in] x The X position of the entity
 * @param[in] y The Y position of the entity
 * @param[in] rad The radius of the entity
 */
bo::Entity::Entity(const double x, const double y, const double rad){
	this->fPos.SetX(x);
	this->fPos.SetY(y);
	this->fRadius = rad;
}

bo::Entity::~Entity() {
	// TODO Auto-generated destructor stub
}

/**
 * Returns true if the entity is colliding with the supplied entity
 *
 * @param[in] ent The entity which might be colliding with our given entity
 *
 * @return True if the ball collides with the supplied entity, false otherwise
 */
bool bo::Entity::CollidesWith(Entity* ent){
	Vector2 distance = (ent->GetPos() - this->GetPos());
	double length = distance.Length();
	return ( length < (this->GetRadius() + ent->GetRadius() ) );
}

/**
 * Returns the position of the entity
 *
 * @return The position of the entity
 */

Vector2 bo::Entity::GetPos(){
	return this->fPos;
}

/**
 * Sets the position of the entity
 *
 * @param[in] x The X position of the entity
 * @param[in] y The Y position of the entity
 */

void bo::Entity::SetPos(double x, double y){
	this->fPos.SetX(x);
	this->fPos.SetY(y);
}

/**
 * Returns the radius of the entity
 *
 * @return The radius of the used entity
 */

double bo::Entity::GetRadius(){
	return this->fRadius;
}

