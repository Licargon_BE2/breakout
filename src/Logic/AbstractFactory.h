/**
* @file AbstractFactory.h
* Abstract Factory (Abstract base class) definition
* @author Timmy Nelen
* @date 27/11/2010
*/
#ifndef ABSTRACTFACTORY_H_
#define ABSTRACTFACTORY_H_

#include "Ball.h"
#include "Breakable.h"
#include "Entity.h"
#include "Paddle.h"
#include "PowerUp.h"
#include "Level.h"
using namespace bo;

/**
* @class AbstractFactory
* @brief Base abstract class
* @author Timmy Nelen
*/
class AbstractFactory {
public:
	virtual Ball* CreateBall(const char* fileName, double xPos, double yPos, double xVel, double yVel) = 0;
	virtual Breakable* CreateBreakable(const char* fileName, double xPos, double yPos) = 0;
	virtual Paddle* CreatePaddle(const char* fileName, double xPos, double yPos, const double vel) = 0;
	virtual PowerUp* CreatePowerUp(const char* fileName, double xPos, double yPos, const double vel, const int probability) = 0;
	virtual double GetBreakableWidth(const char* breakableFile) = 0;
	virtual Level* GetLevel() = 0;
};

#endif /* ABSTRACTFACTORY_H_ */
