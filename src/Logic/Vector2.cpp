/**
* @file Vector2.cpp
* Vector2 implementation
* @author Timmy Nelen
* @date 17/10/2010
*/

#include "Vector2.h"
using namespace std;

Vector2::Vector2(){
	x = 0;
	y = 0;
}
Vector2::Vector2(double x, double y){
	this->x = x;
	this->y = y;
}

double Vector2::GetX() const{
	return x;
}

void Vector2::SetX(double xNew){
	x = xNew;
}

double Vector2::GetY() const{
	return y;
}

void Vector2::SetY(double yNew){
	y = yNew;
}

double Vector2::Length(){
	return sqrt((x*x) + (y*y));
}

void Vector2::FlipX(){
	x = -x;
}

void Vector2::FlipY(){
	y = -y;
}

void Vector2::Normalize(){
	double scale = sqrt ( x*x + y*y );
	this->x = this->x/scale;
	this->y = this->y/scale;
}

double Vector2::Norm(){
	return sqrt ( this->x*this->x + this->y*this->y );
}

double Vector2::DotProd(Vector2 vect2){
	return ( x * y + vect2.GetX() * vect2.GetY() );
}

Vector2& Vector2::ConvToScreenCoords(const double scale){
	this->x = (this->x + 4) * scale;
	this->y = -(this->y - 3) * scale;
	return *this;
}

Vector2& Vector2::operator+(const Vector2& vector2){
	x += vector2.GetX();
	y += vector2.GetY();
	return *this;
}

Vector2& Vector2::operator-(const Vector2& vector2){
	x -= vector2.GetX();
	y -= vector2.GetY();
	return *this;
}

Vector2& Vector2::operator++(int a){
	++x;
	++y;
	return *this;
}

Vector2& Vector2::operator--(int a){
	--x;
	--y;
	return *this;
}

Vector2& Vector2::operator+(const double a){
	x += a;
	y += a;
	return *this;
}

Vector2& Vector2::operator-(const double a){
	x -= a;
	y -= a;
	return *this;
}

Vector2& Vector2::operator*(const double a){
	x *= a;
	y *= a;
	return *this;
}

Vector2& Vector2::operator/(const double a){
	x /= a;
	y /= a;
	return *this;
}

Vector2& Vector2::operator+=(const double a){
	x += a;
	y += a;
	return *this;
}

ostream& operator<<(ostream& os, const Vector2& vector){
	os  << "X: " << vector.GetX() << " Y:" << vector.GetY() << endl;
	return os;
}


