/**
* @file LevelGenerator.h
* LevelGenerator class declaration
* @author Timmy Nelen
* @date 1/1/2010
*/


#ifndef LEVELGENERATOR_H_
#define LEVELGENERATOR_H_

#include "AbstractFactory.h"
#include "Level.h"

namespace bo {

/**
* @class LevelGenerator
* @brief LevelGenerator class, generates standard levels filled with entities
* @author Timmy Nelen
* @date 1/1/2010
*/
class LevelGenerator {
public:
	LevelGenerator(AbstractFactory*);
	virtual ~LevelGenerator();
	AbstractFactory* GetFact();
	virtual Level* CreateStandardLevel(const char*, const char*, const char*, const char*, const char*);
	double GetScale();
private:
	AbstractFactory* fFact;
	double fScale;
};

}

#endif /* LEVELGENERATOR_H_ */
