/**
* @file Stopwatch.cpp
* Stopwatch implementation
* @author Timmy Nelen
* @date 29/11/2010
*/

#include "Stopwatch.h"

/**
 * Constructor initializes the Frames per second
 */
bo::Stopwatch::Stopwatch(const int fps)
	: fFPS(fps), fPrevTime(clock()){

}

/**
 * (Empty) Destructor
 */
bo::Stopwatch::~Stopwatch() {

}
/**
 * Waits till the appropriate time length has been passed, then returns
 */
void bo::Stopwatch::GetTick(){
	clock_t endTime = fPrevTime + (1.0/this->fFPS)*CLOCKS_PER_SEC;
	while (clock() < endTime){}
	fPrevTime = clock();
	return;
}
