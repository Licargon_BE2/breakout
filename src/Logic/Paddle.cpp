/**
* @file Paddle.cpp
* Implementation of the Paddle class
* @author Timmy Nelen
* @date 18/11/2010
*/

#include "Paddle.h"

/**
 * Constructor for a paddle object
 *
 * @param[in] x The X position of the paddle
 * @param[in] y The Y position of the paddle
 * @param[in] rad The radius of the paddle
 * @param[in] vel The velocity of the paddle
 */
bo::Paddle::Paddle(const double x, const double y, const double rad, const double vel)
	: MovableEntity(x, y, rad){
	this->fVelocity = vel;
}

/**
 * Destructor of the paddle object
 */
bo::Paddle::~Paddle() {
	// TODO Auto-generated destructor stub
}


/**
 * Sets the velocity of the paddle. Single because the paddle can only move from left to right.
 *
 * @param[in] vel The velocity of the paddle
 */
void bo::Paddle::SetSingleVelocity(const double vel){
	this->fVelocity = vel;
}

/**
 * Returns the velocity of the paddle
 *
 * @return The velocity of the paddle
 */
double bo::Paddle::GetVelocity(){
	return this->fVelocity;
}

/**
 * Updates the position of the paddle.
 *
 *@param[in] direction The direction in which the paddle must move
 * 		0 - Move paddle left
 * 		1 - Move paddle right
 */
void bo::Paddle::UpdatePos(int direction){
	// Compare the cur pos to the boundaries of the screen to prevent the paddle from going out of bounds
	if ( direction == 0 ){
		if ( this->GetPos().GetX()-this->GetRadius() > -4 )
			this->SetPos(this->GetPos().GetX() - this->fVelocity, this->GetPos().GetY());

	}
	else if ( direction == 1 ){
		if (this->GetPos().GetX() + this->GetRadius() < 4)
			this->SetPos(this->GetPos().GetX() + this->fVelocity, this->GetPos().GetY());
	}

}
