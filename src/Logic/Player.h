/**
* @file Player.H
* Player class declaration
* @author Timmy Nelen
* @date 12/12/2010
*/

#ifndef PLAYER_H_
#define PLAYER_H_

#include <string>
#include <time.h>
#include <sstream>

namespace bo {

/**
* @class Player
* @brief Represents the player playing the game
* @author Timmy Nelen
* @date 12/12/2010
*/
class Player {
public:
	Player(const char* name, const int amOfLives);
	virtual ~Player();
	void AddPoints(const int points);
	int GetLivesLeft();
	const char* GetLivesLeftString();
	void DecLives();
	virtual void Visualize(){};
	const char* GetName();
	int GetScore();
	const char* GetScoreString();
	bool GetPowerUpActive();
	void SetPowerUpActive();
	void AdvancePowerUpTimer(int& returnVal);
private:
	const char* fName;
	int fScore;
	int fLives;
	bool fPowerUpActive;
	double fPrevTime;
};

}

#endif /* PLAYER_H_ */
