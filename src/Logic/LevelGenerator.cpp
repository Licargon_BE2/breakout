/**
* @file LevelGenerator.cpp
* Implementation of the levelgenerator class
* @author Timmy Nelen
* @date 1/12/2010
*/

#include "LevelGenerator.h"

/**
 * Sets the factory to be used in the contructor
 *
 * @param[in] fact The factory to be used
 */
bo::LevelGenerator::LevelGenerator(AbstractFactory* fact) {
	this->fFact = fact;
}

/**
 * Destructor of the levelgenerator object
 */
bo::LevelGenerator::~LevelGenerator() {
	delete fFact;
}

/**
 * Returns the used factory
 *
 * @return The used factory
 */
AbstractFactory* bo::LevelGenerator::GetFact(){
	return this->fFact;
}

/**
 * Creates a pointer to a standard level, filled with the supplied objects
 *
 * @param[in] ballFileName The filename of the ball sprite
 * @param[in] paddleFileName The filename of the paddle sprite
 * @param[in] breakFileName The filename of the breakable sprite
 * @param[in] powFile The filename of the pow sprite
 * @param[in] powerUpFile The filename of the powerup sprite
 *
 * @return The level which is created
 */
Level* bo::LevelGenerator::CreateStandardLevel(const char* ballFileName, const char* paddleFileName, const char* breakFileName,
		const char* powFile, const char* powerUpFile){

	Level* lvl = this->fFact->GetLevel();
	lvl->AddBall(this->fFact->CreateBall(ballFileName, 0, -1.8, 0, .07));
	double dist = fFact->GetBreakableWidth(breakFileName);
	double amInRow = 7.5 / dist;
	lvl->AddPaddle(this->fFact->CreatePaddle(paddleFileName, 0, -2.8, 0.06));
	for (double i = -4 + dist; i < 3.75; i+=(7.5 / amInRow)){
		for (double j = 2.8; j > 0.5; j-=(1*dist)){
			lvl->AddToLevel(this->fFact->CreateBreakable(breakFileName, i, j));
		}
	}
	lvl->AddPowerUp(this->fFact->CreatePowerUp(powerUpFile, 0, 0, 0.04, 15));
	return lvl;
}

/**
 * Returns the scale of the level
 *
 * @return The scale of the used level
 */
double bo::LevelGenerator::GetScale(){
	return this->fScale;
}
