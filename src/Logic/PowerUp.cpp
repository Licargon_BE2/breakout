/**
* @file PowerUp.cpp
* PowerUp class implementation
* @author Timmy Nelen
* @date 13/12/2010
*/

#include "PowerUp.h"

/**
 * Constructor for a powerup object
 *
 * @param[in] x The X position of the powerup
 * @param[in] y The Y position of the powerup
 * @param[in] rad The radius of the powerup
 * @param[in] vel The velocity of the powerup
 * @param[in] probability The probability that the powerup will be released
 */
bo::PowerUp::PowerUp(const double x, const double y, const double rad, const double vel, const int probability)
	: MovableEntity(x, y, rad){
	this->fVel = vel;
	this->fStatus = false;
	this->fProbability = probability;
}

/**
 * The destructor for the powerup object
 */
bo::PowerUp::~PowerUp() {
	// TODO Auto-generated destructor stub
}

/**
 * Releases a powerup at the supplied position
 *
 * @param[in] x The X position of the powerup
 * @param[in] y The Y position of the powerup
 */
void bo::PowerUp::Release(const double x, const double y){
	this->GetPos().SetX(x);
	this->GetPos().SetY(y);
	this->fStatus = false;
}

/**
 * Adds a location to the list of active powerups
 *
 * @param[in] loc The location to be added
 */
void bo::PowerUp::AddLocation(Vector2 loc){
	this->fLocations.push_back(loc);
}

/**
 * Sets the activestatus of the powerup to the supplied bool
 *
 * @param[in] act The status which has to be set
 */
void bo::PowerUp::SetActiveStatus(bool act){
	this->fStatus = act;
}

/**
 * Check wether the powerup is active
 *
 * @return True if the powerup is active
 */
bool bo::PowerUp::IsActive(){
	return this->fStatus;
}

/**
 * Returns the probability of the powerup
 *
 * @return The probability that the powerup will occur
 */
int bo::PowerUp::GetProbability(){
	return fProbability;
}

/**
 * Checks wether the powerup collides with the supplied entity
 *
 * @param[in] ent The entity whose position has to be checked against that of the powerup
 *
 * @return True if the powerup and the entity are colliding
 */
bool bo::PowerUp::CollidesWith(Entity* ent){
	bool collided = false;
	for (list<Vector2>::iterator it = this->fLocations.begin(); it!=this->fLocations.end(); ++it){
		Vector2 tempPos1 = (*it);
		Vector2 tempPos2 = ent->GetPos();
		Vector2 distance = (tempPos1 - tempPos2);
		double length = distance.Length();
		if ( length < (this->GetRadius() + ent->GetRadius() ) ){
			it = this->fLocations.erase(it);
			collided = true;
		}
	}
	return collided;


}

/**
 * Advances the powerup's positions by 1 step
 */
void bo::PowerUp::Advance(){
	for (list<Vector2>::iterator it = this->fLocations.begin(); it!=this->fLocations.end(); ++it){
		(*it).SetY( (*it).GetY() - fVel);
	}
}

/**
 * Returns a list of locations where the powerup is active
 *
 * @return The list of vectors where the powerups are located
 */
list<Vector2> bo::PowerUp::GetLocations(){
	return this->fLocations;
}
