/**
 * @file MovableEntity.h
 * Basis voor een beweegbare entity.
 * @author Timmy Nelen
 * @date 1/1/2011
 */

#ifndef MOVABLEENTITY_H_
#define MOVABLEENTITY_H_

#include "Entity.h"

namespace bo {

/**
* @class MovableEntity
* @brief MovableEntity class, based on Entity class declaration
* @author Timmy Nelen
* @date 1/1/2011
*/
class MovableEntity: public bo::Entity {
public:
	MovableEntity(const double x, const double y, const double rad);
	virtual ~MovableEntity();
	virtual void SetDoubleVelocity(const double xVel, const double yVel){};
	virtual void SetSingleVelocity(const double vel){};
};

}

#endif /* MOVABLEENTITY_H_ */
