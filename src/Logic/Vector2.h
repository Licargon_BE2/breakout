/**
* @file Vector2.h
* Vector2 class declaration, provides an easy to use vector class
* @author Timmy Nelen
* @date 17/10/2010
*/

#ifndef VECTOR2_H_
#define VECTOR2_H_

#include <iostream>
#include <math.h>

/**
* @class Vector2
* @brief Vector2 class declaration
* @author Timmy Nelen
* @date 17/10/2010
*/
class Vector2{
public:
	Vector2();
	Vector2(double, double);
	double GetX() const;
	void SetX(double);
	double GetY() const;
	void SetY(double);
	double Length();
	void FlipX();
	void FlipY();
	double Norm();
	double DotProd(Vector2);
	void Normalize();
	Vector2& ConvToScreenCoords(const double);
	Vector2& operator+(const Vector2&);
	Vector2& operator-(const Vector2&);
	Vector2& operator--(int);
	Vector2& operator++(int);
	Vector2& operator+(const double);
	Vector2& operator-(const double);
	Vector2& operator*(const double);
	Vector2& operator/(const double);
	Vector2& operator+=(const double);
	friend std::ostream& operator<<(std::ostream& os, const Vector2& vector);
private:
	double x;
	double y;
};

#endif /* VECTOR2_H_ */
