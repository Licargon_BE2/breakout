/**
* @file Game.h
* Game Class declaration
* @author Timmy Nelen
* @date 18/11/2010
*/

#ifndef GAME_H_
#define GAME_H_

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "AbstractFactory.h"
#include <vector>
#include "Level.h"
#include "Entity.h"
#include "Ball.h"
#include "Paddle.h"
#include "Stopwatch.h"
#include "LevelGenerator.h"
#include "Player.h"

namespace bo{

/**
* @class Game
* @brief Game Class Declaration
* @author Timmy Nelen
* @date 18/11/2010
*/
class Game {
public:
	Game(AbstractFactory*, LevelGenerator*, const int fps, const int lives, Player* player);
	virtual ~Game();
	void GenStdGame(const char* paddleFile, const char* breakableFile, const char* ballFile, const char* powFile, const char* powerUpFile);
	void AddLevel(Level*);
	void ScreenSize(int, int);
	void Run(int& returnValue);
	void Tick();
private:
	AbstractFactory* fFactory;
	vector<Level*> fLevVector;
	int fScreenWidth;
	int fScreenSize;
	bool fGameRunning;
	Stopwatch* fTimer;
	LevelGenerator* fLvlGen;
	int fLivesLeft;
	Player* fPlayer;
	int fFPS;
};
}

#endif /* GAME_H_ */
