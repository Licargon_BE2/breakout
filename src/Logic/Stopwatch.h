/**
* @file Stopwatch.h
*
* @author Timmy Nelen
* @date 29/11/2010
*/

#ifndef STOPWATCH_H_
#define STOPWATCH_H_

#include <ctime>
using namespace std;

namespace bo{
/**
* @class Stopwatch
* @brief Stopwatch class, used to time the game's ticks
* @author Timmy Nelen
* @date 29/11/2010
*/
class Stopwatch {
public:
	Stopwatch(const int fps);
	virtual ~Stopwatch();
	void GetTick();
private:
	int fFPS;
	double fPrevTime;
};
}

#endif /* STOPWATCH_H_ */
