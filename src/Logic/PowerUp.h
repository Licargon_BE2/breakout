/**
* @file PowerUp.h
* PowerUp class declaration, based on MovableEntity
* @author Timmy Nelen
* @date 13/12/2010
*/


#ifndef POWERUP_H_
#define POWERUP_H_

#include "MovableEntity.h"
#include "Vector2.h"
#include <list>
#include <cstdlib>
using namespace std;

namespace bo {


/**
* @class PowerUp
* @brief PowerUp class, represents powerups used in the game
* @author Timmy Nelen
* @date 13/12/2010
*/
class PowerUp: public bo::MovableEntity {
public:
	PowerUp(const double x, const double y, const double rad, const double vel, const int probability);
	virtual ~PowerUp();
	void UpdatePos();
	virtual void Visualize() = 0;
	void SetActiveStatus(bool act);
	void Release(const double x, const double y);
	bool IsActive();
	void Advance();
	void AddLocation(Vector2 loc);
	list<Vector2> GetLocations();
	bool CollidesWith(Entity* ent);
	int GetProbability();
private:
	double fVel;
	bool fStatus;
	list<Vector2> fLocations;
	int fProbability;
};

}

#endif /* POWERUP_H_ */
