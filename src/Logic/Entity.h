/**
* @file Entity.h
*
* @author Timmy Nelen
* @date 18/11/2010
*/


#ifndef ENTITY_H_
#define ENTITY_H_

#include "Vector2.h"

namespace bo {

/**
* @class Entity
* @brief Base Entity Class
* @author Timmy Nelen
* @date 18/11/2010
*/
class Entity {
public:
	Entity(const double x, const double y, const double rad);
	virtual ~Entity();
	virtual void Visualize() = 0;
	Vector2 GetPos();
	void SetPos(double x, double y);
	bool CollidesWith(Entity* ent);
	double GetRadius();
private:
	Vector2 fPos;
	double fRadius;
};

}

#endif /* ENTITY_H_ */
