/**
* @file Breakable.cpp
*
* @author Timmy Nelen
* @date 18/11/2010
*/

#include "Breakable.h"

/**
 * Constructor of a breakable object
 *
 * @param[in] x The X position of the breakable object
 * @param[in] y The Y position of the breakable object
 * @param[in] rad The radius of the breakable object
 */
bo::Breakable::Breakable(const double x, const double y, const double rad)
	: Entity(x, y, rad){

}

bo::Breakable::~Breakable() {
	// TODO Auto-generated destructor stub
}

